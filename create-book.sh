#!/bin/sh

#Include Credentials File
. ./credentials.sh


# Exit if there is not enough parameters
if [ $# -lt 1 ]
then
  echo "Usage: $0 book-name"
  echo "Please always use lower-case with dash eg: eleven-minutes"
  exit
fi


BOOK_NAME=$1

# Create the repository at BitBucket
curl --user ${BITBUCKET_USER}:${BITBUCKET_PASSWORD} https://api.bitbucket.org/1.0/repositories/ --data name=${BOOK_NAME}

# Create local files and dependencies
mkdir -p books/${BOOK_NAME}
cd books/${BOOK_NAME}
git init
git remote add origin https://${BITBUCKET_GROUP}@bitbucket.org/${BITBUCKET_GROUP}/${BOOK_NAME}.git



git add .

git commit -m "Initial Commit"

#git push -u origin --all # pushes up the repo and its refs for the first time
#git push -u origin --tags # pushes up any tags